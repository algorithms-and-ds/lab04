﻿#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

struct node {
    int value;
    struct node* next;
    struct node* prev;
};

struct list {
    struct node* head;
    struct node* tail;
    int count;
};

struct list* createList() {
    struct list* tmp = (struct list*)malloc(sizeof(struct list));
    if (tmp == NULL) {
        printf("Помилка!Виділити пам'ять для списку не можна\n");
        exit(EXIT_FAILURE);
    }
    tmp->count = 0;
    tmp->head = tmp->tail = NULL;
    return tmp;
}

void deleteList(struct list** list) {
    struct node* tmp = (*list)->head;
    struct node* next = NULL;
    while (tmp) {
        next = tmp->next;
        free(tmp);
        tmp = next;
    }
    free(*list);
    (*list) = NULL;
}

void pushHead(struct list* list, int data) {
    struct node* tmp = (struct node*)malloc(sizeof(struct node));
    if (tmp == NULL) {
        printf("Помилка!Виділити пам'ять для вузла не можна\n");
        exit(EXIT_FAILURE);
    }
    tmp->value = data;
    tmp->next = list->head;
    tmp->prev = NULL;
    if (list->head) {
        list->head->prev = tmp;
    }
    list->head = tmp;
    if (list->tail == NULL) {
        list->tail = tmp;
    }
    list->count++;
}

void deleteHead(struct list* list) {
    if (list->head == NULL) {
        printf("Помилка!Список порожній\n");
        exit(EXIT_FAILURE);
    }
    struct node* prev = list->head;
    list->head = list->head->next;
    if (list->head) {
        list->head->prev = NULL;
    }
    if (prev == list->tail) {
        list->tail = NULL;
    }
    free(prev);
    list->count--;
}

void pushTail(struct list* list, int data) {
    struct node* tmp = (struct node*)malloc(sizeof(struct node));
    if (tmp == NULL) {
        printf("Помилка!Виділити пам'ять для вузла не можна\n");
        exit(EXIT_FAILURE);
    }
    tmp->value = data;
    tmp->next = NULL;
    tmp->prev = list->tail;
    if (list->tail) {
        list->tail->next = tmp;
    }
    list->tail = tmp;
    if (list->head == NULL) {
        list->head = tmp;
    }
    list->count++;
}

void deleteTail(struct list* list) {
    if (list->tail == NULL) {
        printf("Помилка!Список порожній\n");
        exit(EXIT_FAILURE);
    }
    struct node* prev = list->tail->prev;
    if (prev) {
        prev->next = NULL;
    }
    if (list->tail == list->head) {
        list->head = NULL;
    }
    free(list->tail);
    list->tail = prev;
    list->count--;
}

void pushInside(struct list* list, int count, int data) {
    if (count < 1 || count > list->count + 1) {
        printf("Помилка!Такого місця в цьому списку не існує\n");
        exit(EXIT_FAILURE);
    }
    if (count == 1) {
        pushHead(list, data);
        return;
    }
    if (count == list->count + 1) {
        pushTail(list, data);
        return;
    }
    struct node* current = list->head;
    for (int i = 1; i < count; i++) {
        current = current->next;
    }
    struct node* newNode = (struct node*)malloc(sizeof(struct node));
    if (newNode == NULL) {
        printf("Помилка!Виділити пам'ять для вузла не можна\n");
        exit(EXIT_FAILURE);
    }
    newNode->value = data;
    newNode->next = current;
    newNode->prev = current->prev;
    current->prev->next = newNode;
    current->prev = newNode;
    list->count++;
}

void deleteInside(struct list* list, int count) {
    if (count < 1 || count > list->count) {
        printf("Помилка!Такого місця в цьому списку не існує\n");
        exit(EXIT_FAILURE);
    }
    if (count == 1) {
        deleteHead(list);
        return;
    }
    if (count == list->count) {
        deleteTail(list);
        return;
    }
    struct node* current = list->head;
    for (int i = 1; i < count; i++) {
        current = current->next;
    }
    current->prev->next = current->next;
    current->next->prev = current->prev;
    free(current);
    list->count--;
}



void printList(struct list* list) {
    struct node* tmp = list->head;
    printf(" -------------\n");
    while (tmp) {
        printf("%d\n", tmp->value);
        tmp = tmp->next;
    }
    printf(" -------------\n");
}

int main() {
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    struct list* list = createList();
    pushTail(list, 10);
    pushTail(list, 20);
    pushTail(list, 30);
    pushTail(list, 40);
    pushTail(list, 50);
    pushTail(list, 60);
    pushTail(list, 70);
    pushTail(list, 80);
    pushTail(list, 90);
    int menu = 0;
    do {
        printf("Оберіть дію:\n1. Додати на початок\n2. Видалити початок\n3. Додати в кінець\n4. Видалити кінець\n5. Додати в середину\n6. Видалити з середини\n7. Вивести весь список\n8. Видалити весь список та завершити операції\n0. Завершити виконання операцій\n");
        scanf("%d", &menu);
        switch (menu) {
        case 1: {
            int num;
            printf("\nВведіть число: ");
            scanf("%d", &num);
            pushHead(list, num);
            printf("\n\n");
            printList(list);
            break;
        }
        case 2: {
            deleteHead(list);
            printf("\n\n");
            printList(list);
            break;
        }
        case 3: {
            int num;
            printf("\nВведіть число: ");
            scanf("%d", &num);
            pushTail(list, num);
            printf("\n\n");
            printList(list);
            break;
        }
        case 4: {
            deleteTail(list);
            printf("\n\n");
            printList(list);
            break;
        }
        case 5: {
            int num, count;
            printf("\nВведіть число: ");
            scanf("%d", &num);
            printf("Місце: ");
            scanf("%d", &count);
            pushInside(list, count, num);
            printf("\n\n");
            printList(list);
            break;
        }
        case 6: {
            int count;
            printf("Введіть місце: ");
            scanf("%d", &count);
            deleteInside(list, count);
            printf("\n\n");
            printList(list);
            break;
        }
        case 7: {
            printList(list);
            break;
        }
        case 8: {
            deleteList(&list);
            menu = 0;
            break;
        }
        }
    } while (menu != 0);

    return 0;
}


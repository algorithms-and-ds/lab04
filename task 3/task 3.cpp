﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>

#define MAX_SIZE 100

int Priority(char symbol) {
    switch (symbol) {
    case '(': return 0;
    case ')': return 1;
    case '+': return 2;
    case '-': return 3;
    case '*': return 4;
    case '/': return 4;
    case '^': return 5;
    default: return 6;
    }
}

int isDigit(char c) {
    return (c >= '0' && c <= '9');
}

void Station(const char* symbols, char* result) {
    char stack[MAX_SIZE];
    int stack_top = -1;
    int result_top = -1;
    int i = 0;
    while (symbols[i] != '\0') {
        if (isDigit(symbols[i])) {
            while (isDigit(symbols[i]) || symbols[i] == '.') {
                result[++result_top] = symbols[i];
                i++;
            }
            result[++result_top] = ' ';
        }
        else {
            if (symbols[i] == '(') {
                stack[++stack_top] = symbols[i];
            }
            else if (symbols[i] == ')') {
                while (stack_top >= 0 && stack[stack_top] != '(') {
                    result[++result_top] = stack[stack_top--];
                    result[++result_top] = ' ';
                }
                stack_top--;
            }
            else {
                while (stack_top >= 0 && Priority(symbols[i]) <= Priority(stack[stack_top])) {
                    result[++result_top] = stack[stack_top--];
                    result[++result_top] = ' ';
                }
                stack[++stack_top] = symbols[i];
            }
            i++;
        }
    }
    while (stack_top >= 0) {
        result[++result_top] = stack[stack_top--];
        result[++result_top] = ' ';
    }
    result[++result_top] = '\0';
}

int evaluateRPN(char* expression) {
    int stack[MAX_SIZE];
    int stack_top = -1;
    char* token = strtok(expression, " ");
    while (token != NULL) {
        if (isDigit(token[0]) || (token[0] == '-' && isDigit(token[1]))) {
            stack[++stack_top] = atoi(token);
        }
        else {
            int b = stack[stack_top--];
            int a = stack[stack_top--];
            switch (token[0]) {
            case '+': stack[++stack_top] = a + b; break;
            case '-': stack[++stack_top] = a - b; break;
            case '*': stack[++stack_top] = a * b; break;
            case '/': stack[++stack_top] = a / b; break;
            case '^': stack[++stack_top] = (int)pow(a, b); break;
            default: printf("Unknown operator: %s\n", token); exit(1);
            }
        }
        token = strtok(NULL, " ");
    }
    return stack[stack_top];
}

int main() {
    SetConsoleCP(1251); SetConsoleOutputCP(1251);
    char expression[MAX_SIZE];
    printf("Введіть вираз (кожен символ через пробіл):\n");
    fgets(expression, MAX_SIZE, stdin);
    expression[strcspn(expression, "\n")] = '\0'; 
    char expressionList[MAX_SIZE];
    Station(expression, expressionList);
    printf("Зворотній польський запис: %s\n", expressionList);
    char rpnExpression[MAX_SIZE];
    strcpy(rpnExpression, expressionList);
    int result = evaluateRPN(rpnExpression);
    printf("Результат обчислення: %d\n", result);
    return 0;
}
